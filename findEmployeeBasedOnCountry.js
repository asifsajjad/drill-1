// const data = require('./data.json');
const getCorrectedSalary = require('./getCorrectedSalary');

function findEmployeeBasedOnCountry(data) {

    const newData = getCorrectedSalary(data);
    const output = {};
    for (obj of newData) {
        let country = obj.location;
        if (!output[country]) {
            output[country] = 1;
        }
        else {
            output[country] += 1;
        }

    }

    return output;

}

// console.log(findEmployeeBasedOnCountry(data));

module.exports=findEmployeeBasedOnCountry;