// const data= require('./data.json');

function findWebDeveloper(data) {

    const output = [];

    for (let obj of data) {
        let searchString = obj.job;
        if (searchString.includes('Web Developer')) {
            output.push(obj);
        }
    }

    return output;

}

module.exports = findWebDeveloper;
// console.log(findWebDeveloper(data));