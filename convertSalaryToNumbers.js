// const data= require('./data.json');

function convertSalaryToNumbers(data) {

    const output = [];
    for (obj of data) {
        let salaryString = obj.salary;
        if (!typeof salaryString === Number) {
            let newSalaryString = salaryString.replace('$', '');
            let salaryNum = parseFloat(newSalaryString);
            obj.salary = salaryNum;
        }
        output.push(obj);
    }

    return output;

}

module.exports = convertSalaryToNumbers;
// console.log(convertSalaryToNumbers(data));