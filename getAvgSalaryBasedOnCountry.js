const data = require('./data.json');

const findEmployeeBasedOnCountry = require("./findEmployeeBasedOnCountry");
const getSumOfSalaryBasedOnCountry = require("./getSumOfSalaryBasedOnCountry");

function getAvgSalaryBasedOnCountry(data){

    const countryEmployees= findEmployeeBasedOnCountry(data);
    console.log(data);
    const countrySalary= getSumOfSalaryBasedOnCountry(data);
    
    const output= {};
    for(let countryName in countrySalary){
        console.log(countryName);
        let totalSalary= countrySalary.countryName;
        console.log(totalSalary)
        let totalEmployees= countryEmployees.countryName;
        console.log(totalEmployees);
        let avgSalary= totalSalary/totalEmployees;
        output[countryName]=avgSalary;
    }

    return output;
}

console.log(getAvgSalaryBasedOnCountry(data));