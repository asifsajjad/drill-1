// const data = require('./data.json');
const getCorrectedSalary = require('./getCorrectedSalary');

function getSumOfSalaryBasedOnCountry(data) {

    const newData = getCorrectedSalary(data);
    const output = {};
    for (obj of newData) {
        let salary = obj.corrected_salary;
        let country = obj.location;
        if (!output[country]) {
            output[country] = salary;
        }
        else {
            output[country] += salary;
        }

    }

    return output;

}

// console.log(getSumOfSalaryBasedOnCountry(data));

module.exports= getSumOfSalaryBasedOnCountry;