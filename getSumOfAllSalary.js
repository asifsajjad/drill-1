// const data= require('./data.json');
const getCorrectedSalary = require('./getCorrectedSalary');

function getSumOfAllSalary(data){

    const newData=getCorrectedSalary(data);
    let output = 0;
    for(obj of newData){
        let salary= obj.corrected_salary;
        output +=salary;
        
    }

    return output;

}

// console.log(getSumOfAllSalary(data));

module.exports=getSumOfAllSalary;