const convertSalaryToNumbers = require('./convertSalaryToNumbers');
// const data= require('./data.json');

function getCorrectedSalary(data){

    const newData=convertSalaryToNumbers(data);
    const output =[];
    for(obj of newData){
        let oldSalary= obj.salary;
        obj['corrected_salary']=oldSalary*10000;
        output.push(obj);
    }

    return output;

}

// console.log(getCorrectedSalary(data));

module.exports=getCorrectedSalary;